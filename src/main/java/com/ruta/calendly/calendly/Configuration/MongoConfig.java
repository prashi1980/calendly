package com.ruta.calendly.calendly.Configuration;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
public class MongoConfig {

    @Value("${mongo.db.url}")
    private String mongoUrl;

    @Value("${mongo.db.name}")
    private String databaseName;

    @Bean
    public MongoClient mongo() {
        MongoClientURI uri = new MongoClientURI(mongoUrl);
        return new MongoClient(uri);
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongo(), databaseName);
    }

}
