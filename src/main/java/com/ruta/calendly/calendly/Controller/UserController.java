package com.ruta.calendly.calendly.Controller;


import com.ruta.calendly.calendly.Dto.*;
import com.ruta.calendly.calendly.Enums.ErrorCode;
import com.ruta.calendly.calendly.Exceptions.BadRequestException;
import com.ruta.calendly.calendly.Exceptions.NotFoundException;
import com.ruta.calendly.calendly.Exceptions.UnauthorizedException;
import com.ruta.calendly.calendly.Model.User;
import com.ruta.calendly.calendly.Service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashMap;

@Controller
@Slf4j
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(path = "/signup", consumes = "application/json", method = RequestMethod.POST)
    public ResponseEntity<UserResponseDto> signUpUser(@RequestBody UserSignupRequestDto userSignupRequestDto) throws BadRequestException, UnauthorizedException {

        if (StringUtils.isBlank(userSignupRequestDto.getEmail()) || StringUtils.isBlank(userSignupRequestDto.getPassword())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST);
        }
        User user = new User(userSignupRequestDto.getEmail(), userSignupRequestDto.getPassword(), userSignupRequestDto.getName(), new HashMap<>());
        UserResponseDto userResponseDto = userService.signUp(user);
        return new ResponseEntity(userResponseDto, HttpStatus.OK);
    }

    @RequestMapping(path = "/login", consumes = "application/json", method = RequestMethod.POST)
    public ResponseEntity<UserResponseDto> loginUser(@RequestBody UserLoginRequestDto userLoginRequestDto) throws UnauthorizedException {
        User user = new User(userLoginRequestDto.getEmail(), userLoginRequestDto.getPassword(), new String(), new HashMap<>());
        UserResponseDto userResponseDto = userService.login(user);
        return new ResponseEntity(userResponseDto, HttpStatus.OK);
    }

    @RequestMapping(path = "/freeSlots", consumes = "application/json", method = RequestMethod.POST)
    public ResponseEntity<UserResponseDto> freeUserSlots(@RequestBody UserSlotsRequestDto userLoginRequestDto,
                                                         @RequestHeader(name = "token", required = true) String token) throws UnauthorizedException, BadRequestException {

        UserResponseDto userResponseDto = userService.freeSlots(userLoginRequestDto.getSlots(), token);
        return new ResponseEntity<>(userResponseDto, HttpStatus.OK);
    }

    @RequestMapping(path = "/bookSlots", consumes = "application/json", method = RequestMethod.POST)
    public ResponseEntity<UserResponseDto> bookUserSlots(@RequestBody UserBookSlotsRequestDto userBookSlotsRequestDto,
                                                         @RequestHeader(name = "token", required = true) String token) throws NotFoundException, BadRequestException, UnauthorizedException {

        UserResponseDto userResponseDto = userService.bookSlots(userBookSlotsRequestDto, token);
        return new ResponseEntity<>(userResponseDto, HttpStatus.OK);
    }

    @RequestMapping(path = "/mySchedule", method = RequestMethod.GET)
    public ResponseEntity<UserResponseDto> userSchedule(@RequestHeader(name = "token", required = true) String token) throws UnauthorizedException {
        UserResponseDto userResponseDto = userService.userBookingSchedule(token);
        return new ResponseEntity<>(userResponseDto, HttpStatus.OK);
    }


}


