package com.ruta.calendly.calendly.Dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@Getter
@AllArgsConstructor
public class UserErrorResponseDto {

    private final long timeStamp = System.currentTimeMillis();
    private String errorCode;
    private String errorMessage;
    private Map<String, Object> metaData;
}
