package com.ruta.calendly.calendly.Dto;

import lombok.Getter;

@Getter

public class UserSignupRequestDto {

    private String name;
    private String email;
    private String password;

}
