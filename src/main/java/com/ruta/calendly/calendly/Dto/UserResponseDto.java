package com.ruta.calendly.calendly.Dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@AllArgsConstructor
public class UserResponseDto {

    private final long timeStamp = System.currentTimeMillis();
    private Map<String, Object> metaData;

}
