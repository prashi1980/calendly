package com.ruta.calendly.calendly.Service;

import com.ruta.calendly.calendly.Dao.MongoDao;
import com.ruta.calendly.calendly.Dto.UserBookSlotsRequestDto;
import com.ruta.calendly.calendly.Dto.UserResponseDto;
import com.ruta.calendly.calendly.Enums.ErrorCode;
import com.ruta.calendly.calendly.Exceptions.BadRequestException;
import com.ruta.calendly.calendly.Exceptions.NotFoundException;
import com.ruta.calendly.calendly.Exceptions.UnauthorizedException;
import com.ruta.calendly.calendly.Model.User;
import com.ruta.calendly.calendly.Util.UserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class UserService {

    @Autowired
    private MongoDao mongoDao;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Value("${password.salt}")
    private String salt;

    public UserResponseDto signUp(User user) throws UnauthorizedException {

        User userExsists = mongoDao.findByEmail(user.getEmail());
        if(userExsists == null){
            String encryptedPassword = UserUtil.encryptPassword(user.getPassword(), salt);
            user.setPassword(encryptedPassword);
            mongoDao.saveUser(user);
            Map<String, Object> metaData = new HashMap<>();
            metaData.put("message", "User registered");
            return new UserResponseDto(metaData);
        } else {
            throw new UnauthorizedException(ErrorCode.USER_ALREADY_EXISTS);
        }

    }

    public UserResponseDto login(User user) throws UnauthorizedException {

        String encryptedPassword = UserUtil.encryptPassword(user.getPassword(), salt);
        user.setPassword(encryptedPassword);
        User userFromDB = mongoDao.findByEmail(user.getEmail());

        if (userFromDB != null && encryptedPassword.equals(userFromDB.getPassword())) {
            Map<String, String> map = new HashMap<>();
            map.put("email", userFromDB.getEmail());
            String token = jwtTokenService.createJwtToken(map);
            Map<String, Object> metaData = new HashMap<>();
            metaData.put("token", token);
            UserResponseDto userResponseDto = new UserResponseDto(metaData);
            return userResponseDto;

        } else {
            throw new UnauthorizedException(ErrorCode.CREDENTIAL_MISMATCH);
        }
    }

    public UserResponseDto freeSlots(ArrayList<Integer> slots, String token) throws BadRequestException, UnauthorizedException {
        Map<String, Object> tokenData = jwtTokenService.validateJwtToken(token);
        if (!CollectionUtils.isEmpty(tokenData)) {
            if (UserUtil.areTimeSlotsValid(slots)) {
                String email = (String) tokenData.get("email");
                User userFromDB = mongoDao.findByEmail(email);
                Map<String, Object> freeSlots = UserUtil.getTimeSlots(slots);
                userFromDB.setSlots(freeSlots);
                mongoDao.updateUser(userFromDB);
                Map<String, Object> metaData = new HashMap<>();
                metaData.put("message", "user slots registered");
                UserResponseDto userResponseDto = new UserResponseDto(metaData);
                return userResponseDto;
            } else {
                throw new UnauthorizedException(ErrorCode.BAD_REQUEST);
            }
        } else {
            throw new BadRequestException(ErrorCode.UNAUTHORIZED_TOKEN);
        }
    }

    public UserResponseDto bookSlots(UserBookSlotsRequestDto userBookSlotsRequestDto, String token) throws UnauthorizedException, NotFoundException, BadRequestException {
        Map<String, Object> tokenData = jwtTokenService.validateJwtToken(token);
        if (!CollectionUtils.isEmpty(tokenData)) {
            //validate email user
            User user = mongoDao.findByEmail(userBookSlotsRequestDto.getEmail());
            if (user != null) {
                // check if slot is valid number
                if (UserUtil.isTimeslotValid(userBookSlotsRequestDto.getSlot())) {
                    Map<String, Object> updatedSlots = user.getSlots();
                    //check if slot is present in user
                    if (updatedSlots.containsKey(userBookSlotsRequestDto.getSlot().toString())) {
                        //check if time slot is available (FREE)
                        if (updatedSlots.get(userBookSlotsRequestDto.getSlot().toString()).equals("FREE")) {
                            updatedSlots.put(userBookSlotsRequestDto.getSlot().toString(), tokenData.get("email"));
                            //update db with new slot
                            user.setSlots(updatedSlots);
                            mongoDao.updateUser(user);
                            //send response Dto
                            Map<String, Object> metaData = new HashMap<>();
                            metaData.put("message", "Your slot is booked!");
                            UserResponseDto userResponseDto = new UserResponseDto(metaData);
                            return userResponseDto;
                        } else {
                            throw new BadRequestException(ErrorCode.TIME_SLOT_NOT_AVAILABLE);
                        }
                    } else {
                        throw new BadRequestException(ErrorCode.TIME_SLOT_NOT_FOUND);
                    }
                } else {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST);
                }
            } else {
                throw new NotFoundException(ErrorCode.USER_NOT_FOUND);
            }

        } else {
            throw new UnauthorizedException(ErrorCode.UNAUTHORIZED_TOKEN);
        }
    }


    public UserResponseDto userBookingSchedule(String token) throws UnauthorizedException {
        Map<String, Object> tokenData = jwtTokenService.validateJwtToken(token);
        if (!CollectionUtils.isEmpty(tokenData)) {
            Map<String, Object> metaData = new HashMap<>();
            User user = mongoDao.findByEmail(tokenData.get("email").toString());
            metaData.put("schedule", user.getSlots());
            UserResponseDto userResponseDto = new UserResponseDto(metaData);
            return userResponseDto;
        } else {
            throw new UnauthorizedException(ErrorCode.UNAUTHORIZED_TOKEN);
        }
    }


}
