package com.ruta.calendly.calendly.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.ruta.calendly.calendly.Enums.ErrorCode;
import com.ruta.calendly.calendly.Exceptions.UnauthorizedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
@Slf4j
public class JwtTokenService {

    private final String ISSUER = "calendly";

    @Value("${jwt.secret}")
    private String jwtSecret;

    public String createJwtToken(Map<String, String> claim) {
        try {
            JWTCreator.Builder builder = com.auth0.jwt.JWT.create();
            builder.withIssuer(ISSUER);
            builder.withExpiresAt(new Date(System.currentTimeMillis() + 3600000));
            builder.withJWTId(String.valueOf(UUID.randomUUID()));

            for (Map.Entry<String, String> item : claim.entrySet()) {
                builder.withClaim(item.getKey(), item.getValue());
            }
            Algorithm algorithm = Algorithm.HMAC256(jwtSecret);
            return builder.sign(algorithm);

        } catch (JWTCreationException exception) {
            //Invalid Signing configuration / Couldn't convert Claims.
            log.error("Error while creating jwt token: {}", exception);
            throw exception;
        }

    }

    //retun hashmap
    public Map<String, Object> validateJwtToken(String token) throws UnauthorizedException {

        try {
            Algorithm algorithm = Algorithm.HMAC256(jwtSecret);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer(ISSUER)
                    .build(); //Reusable verifier instance
            DecodedJWT jwt = verifier.verify(token);

            Map<String, Claim> claims = jwt.getClaims();
            Claim claim = claims.get("email");
            Map<String, Object> tokenData = new HashMap<>();
            tokenData.put("email", claim.asString());
            return tokenData;

        } catch (JWTVerificationException exception) {
            //Invalid signature/claims
            log.error("Invalid token");
            throw new UnauthorizedException(ErrorCode.UNAUTHORIZED_TOKEN);
        }
    }
}
