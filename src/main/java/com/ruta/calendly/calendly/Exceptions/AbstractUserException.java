package com.ruta.calendly.calendly.Exceptions;

import com.ruta.calendly.calendly.Enums.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public abstract class AbstractUserException extends Exception{
    private final ErrorCode errorCode;


}
