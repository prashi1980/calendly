package com.ruta.calendly.calendly.Exceptions;

import com.ruta.calendly.calendly.Enums.ErrorCode;
import lombok.Getter;

@Getter
public class UnauthorizedException extends AbstractUserException {

    public UnauthorizedException(ErrorCode errorCode) {
        super(errorCode);
    }
}
