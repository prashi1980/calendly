package com.ruta.calendly.calendly.Exceptions;

import com.ruta.calendly.calendly.Enums.ErrorCode;

public class BadRequestException extends AbstractUserException {


    public BadRequestException(ErrorCode errorCode) {
        super(errorCode);
    }
}
