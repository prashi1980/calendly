package com.ruta.calendly.calendly.Handlers;

import com.ruta.calendly.calendly.Dto.UserErrorResponseDto;
import com.ruta.calendly.calendly.Enums.ErrorCode;
import com.ruta.calendly.calendly.Exceptions.AbstractUserException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Collections;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorCode errorCode = ErrorCode.UNHANDLED_EXCEPTION;
        UserErrorResponseDto errorResponseDto = new UserErrorResponseDto(errorCode.getErrorCode(), errorCode.getErrorMessage(), Collections.EMPTY_MAP);
        return new ResponseEntity<>(errorResponseDto, errorCode.getHttpStatus());
    }

    @ExceptionHandler(AbstractUserException.class)
    protected ResponseEntity<UserErrorResponseDto> handleUserExceptions(AbstractUserException e) {
        ErrorCode errorCode = e.getErrorCode();
        UserErrorResponseDto errorResponseDto = new UserErrorResponseDto(errorCode.getErrorCode(), errorCode.getErrorMessage(), Collections.EMPTY_MAP);
        return new ResponseEntity<>(errorResponseDto, errorCode.getHttpStatus());
    }

}