package com.ruta.calendly.calendly.Util;


import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class UserUtil {

    public static String encryptPassword(String data, String salt) {
        if (StringUtils.isBlank(data)) {
            return "";
        }

        String encodedSalt = Base64.getEncoder().encodeToString(salt.getBytes(Charset.forName("UTF-8")));
        return DigestUtils.sha256Hex(data + encodedSalt);
    }

    public static boolean areTimeSlotsValid(ArrayList<Integer> slots) {

        for (int i = 0; i < slots.size(); i++) {
            if (slots.get(i) > 24 || slots.get(i) < 0) {
                return false;
            }
        }
        return true;
    }

    public static Map<String, Object> getTimeSlots(ArrayList<Integer> slots) {
        Map<String, Object> freeSlots = new HashMap<>();
        for (int i = 0; i < slots.size(); i++) {
            String slot = slots.get(i).toString();
            freeSlots.put(slot, "FREE");
        }
        return freeSlots;
    }

    public static boolean isTimeslotValid(Integer timeSlot) {

        return (timeSlot < 24 && timeSlot >= 0);
    }


}

