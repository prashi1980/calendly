package com.ruta.calendly.calendly.Dao;

import com.ruta.calendly.calendly.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;

@Repository
public class MongoDao {

    @Autowired
    MongoTemplate mongoTemplate;

    public void saveUser(User user){
        mongoTemplate.save(user);
    }

    public User findByEmail(String email){
        Query query = new Query();
        query.addCriteria(Criteria.where("email").is(email));
        List<User> users = mongoTemplate.find(query, User.class);
        return CollectionUtils.isEmpty(users) ? null : users.get(0);
    }


    public void updateUser(User user){
        Query query = new Query();
        query.addCriteria(Criteria.where("email").is(user.getEmail()));
        Update update = new Update();
        update.set("slots", user.getSlots());
        mongoTemplate.updateFirst(query, update, User.class);
    }





}
